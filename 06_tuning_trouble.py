def read_file(filename):
	with open(filename, 'r') as file:
		return file.read().splitlines()

def find_sequence(line: str) -> int:
	for pos, character in enumerate(line):
		if len(set(line[pos:pos + 4])) == len(line[pos:pos + 4]):
			return pos + 4

def find_sequence_part2(line: str) -> int:
	for pos, character in enumerate(line):
		if len(set(line[pos:pos + 14])) == len(line[pos:pos + 14]):
			return pos + 14

def show_start_of_packet(filename) -> int:
		return [find_sequence(line) for line in read_file(filename)]

def show_start_of_packet_part2(filename) -> int:
		return [find_sequence_part2(line) for line in read_file(filename)]

if __name__ == '__main__':
	filename = 'test_data.txt'
	filename = 'puzzleinput.txt'

	print(*show_start_of_packet(filename), sep='\n')
	print('\n', *show_start_of_packet_part2(filename), sep='\n')
