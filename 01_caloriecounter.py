def calculate_calories(filename):
	with open(filename, 'r') as file:
		data = file.read().splitlines()

	calories_sums = []
	current_block_calories = 0

	for d in data:
		if d == '':
			calories_sums.append(current_block_calories)
			current_block_calories = 0
		elif d == data[-1]:
			current_block_calories += int(d)
			calories_sums.append(current_block_calories)
		else:
			current_block_calories += int(d)

	calories_sums.sort(reverse=True)
	return calories_sums

if __name__ == '__main__':

	file = 'puzzleinput.txt'
	calories_sums = calculate_calories(file)

	print(f"Top Calorie Score:\t{calories_sums[0]}")
	print(f"Sum of top three calorie scores:\t{sum(calories_sums[0:3])}")
