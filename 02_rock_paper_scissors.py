"""Notes:Part 1	A, X: Rock 1 Point
				B, Y: Paper 2 Points
				C, Z: Scissors 3 Points
				Lost: 0 Points
				Draw: 3 Points
				Win: 6 Points
		Part 2	A, A: Rock 1 Point
				B, B: Paper 2 Points
				C, C: Scissors 3 Points
				X: Lose 0 Points
				Y: Draw 3 Points
				Z: Win 6 Points"""

def calculate_points_part1(filename):
	with open(filename, 'r') as file:
		data = file.read().splitlines()

	win_lose = {'A Y': 6, 'B Z': 6, 'C X': 6,
		'A Z': 0,'B X': 0,'C Y': 0,
		'draw': 3}
	rock_paper_scissors = {'X': 1, 'Y': 2, 'Z':3}

	points = 0
	for d in data:
		my_choice = d[-1]
		try:
			points += win_lose[d] + rock_paper_scissors[my_choice]
		except KeyError:
			points += win_lose['draw'] + rock_paper_scissors[my_choice]

	return points

def calculate_points_part2(filename):
	with open(filename, 'r') as file:
		data = file.read().splitlines()

	rock_paper_scissors = {'A X': 3, 'B X': 1, 'C X': 2, # Lose
							'A Y': 4, 'B Y': 5, 'C Y': 6, # Draw
							'A Z': 8, 'B Z': 9, 'C Z': 7, # Win
							}

	points = 0
	for d in data:
		points += rock_paper_scissors[d]

	return points

if __name__ == '__main__':
	filename = 'puzzleinput.txt'
	points_sum = calculate_points_part2(filename)
	print(points_sum)
