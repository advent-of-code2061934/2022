def read_file(filename) -> list[str]:
	with open(filename, 'r') as file:
		return file.read().splitlines()

def find_indices(line: str, target: str) -> list[int]:
	return [idx for idx, value in enumerate(line) if value == target]

def build_file_tree(filename: list[str]) -> dict[str:int]:
	current_folder = str()
	folders = dict()
	for line in read_file(filename):
		if '$ cd ' in line:
			if line.split()[-1] == '..':
				current_folder = '/'.join(current_folder.split('/')[:-1])
			else:
				if line.split()[-1] == '/':
					current_folder = ''
				else:
					current_folder += '/' + line.split()[-1]
				folders[current_folder] = int()
		elif line.split()[0].isnumeric():
			folders[current_folder] += int(line.split()[0])
			indices = sorted(find_indices(current_folder, '/'), reverse=True)
			for index in indices:
				parent_folder = current_folder[0:index]
				folders[parent_folder] += int(line.split()[0])
	return folders

def solve_puzzle_part1(filename: list[str]) -> int:
	return sum([value for key, value in build_file_tree(filename).items() if value <= 100000])

def solve_puzzle_part2(filename:list[str]) -> int:
	free_space = 30_000_000 - (70_000_000 - build_file_tree(filename)[''])
	return sorted([size for key, size in build_file_tree(filename).items() if size >= free_space])[0]

if __name__ == '__main__':
	filename = 'test_data.txt'
	filename = 'puzzleinput.txt'

	print(solve_puzzle_part1(filename))
	print(solve_puzzle_part2(filename))

