def read_file(filename):
	with open(filename, 'r') as file:
		return file.read().splitlines()

def split_string(line: str) -> tuple[str, str]:
	return line[:len(line) // 2], line[len(line) // 2:]

def priority(letter: str) -> int:
	if letter.isupper():
		return ord(letter) - ord('A') + 27
	else:
		return ord(letter) - ord('a') + 1

def priority_line(line: str) -> int:
	l1, l2 = split_string(line)
	common = set(l1).intersection(set(l2))
	return priority(common.pop())

def common_item(line1: str, line2: str, line3: str) -> int:
	commons12 = set(line1).intersection(set(line2))
	common123 = commons12.intersection(set(line3))
	return priority(common123.pop())

def get_total_priority(lines: list[str]) -> int:
	return sum(priority_line(line) for line in lines)

def get_total_priority_part2(lines:list[str]) -> int:
	total_priority = 0
	for i in range(len(lines) // 3):
		total_priority += common_item(lines[3*i], lines[3*i+1], lines[3*i+2])
	return total_priority

if __name__ == '__main__':
	filename = "puzzleinput.txt"
	lines = read_file(filename)
	print(get_total_priority(lines))
	print(get_total_priority_part2(lines))
