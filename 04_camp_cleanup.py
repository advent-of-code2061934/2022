def read_file(filename):
	with open(filename, 'r') as file:
		return file.read().splitlines()

def cleanse_data(line: str) -> list[list[int],list[int]]:
	result = []
	for r in line.split(','):
		start, end = map(int, r.split('-'))
		result.append(list(range(start, end + 1)))
	return result

def identify_overlaps(line: str) -> int:
	l = cleanse_data(line)
	if set(l[0]).issubset(set(l[1])) or set(l[1]).issubset(set(l[0])):
		return 1
	else:
		return 0

def identify_overlaps_part2(line: str) -> int:
	l = cleanse_data(line)
	if set(l[0]).intersection(set(l[1])):
		return 1
	else:
		return 0

def count_overlaps(data: list[str]) -> int:
	overlaps = 0
	for i in data:
		overlaps += identify_overlaps(i)
	return overlaps

def count_overlaps_part2(data: list[str]) -> int:
	overlaps = 0
	for i in data:
		overlaps += identify_overlaps_part2(i)
	return overlaps

if __name__ == '__main__':
	filename = 'puzzleinput.txt'
	data = read_file(filename)
	print(count_overlaps(data))
	print(count_overlaps_part2(data))
