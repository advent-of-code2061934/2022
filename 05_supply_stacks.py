def read_file(filename) -> list[str]:
	with open(filename, 'r') as file:
		return file.read().splitlines()

def split_crates_orders_position(data: list[str]) -> tuple[str]:
	split_index = data.index('')
	return data[:split_index - 1], data[split_index + 1:], data[split_index - 1] # crates, orders, floor_pos

def cleanse_strings(line: str) -> str:
	return line.replace('[',' ').replace(']',' ')

def rows_to_columns(crates: list[str], floor_pos: list[str]) -> list[list[str]]:
	stacks = []
	for n in range(0, len(floor_pos)):
		stack = []
		for crate in crates:
			if cleanse_strings(crate)[n] != ' ':
				stack.append(cleanse_strings(crate)[n])
		if stack != []:
			stacks.append(stack)
	return stacks

def turn_order_to_index(floor_position: str, order_line: str) -> tuple[int]:
	order = [character for character in order_line if character.isnumeric()]
	if len(order) > 3:
		popped = order.pop(1)
		order[0] += popped
	return int(order[0]), int(order[1]) - 1, int(order[2]) - 1

def move_crates(data: list[str]) -> list[list[str]]:
	crates, orders, floor_pos = split_crates_orders_position(data)
	stacks = rows_to_columns(crates, floor_pos)
	for order in orders:
		reps, take, bring = turn_order_to_index(floor_pos, order)
		for n in range(0, reps):
			popped = stacks[take].pop(0)
			stacks[bring].insert(0, popped)
	return stacks

def move_crates_part2(data: list[str]) -> list[list[str]]:
	crates, orders, floor_pos = split_crates_orders_position(data)
	stacks = rows_to_columns(crates, floor_pos)
	for order in orders:
		reps, take, bring = turn_order_to_index(floor_pos, order)
		popped = [stacks[take].pop(0) for n in range(0, reps)]
		for pos, element in enumerate(popped):
			stacks[bring].insert(pos, element)
	return stacks

def show_top_crates(data: list[str]) -> str:
	stacks = move_crates(data)
	return [crate.pop(0) for crate in stacks]

def show_top_crates_part2(data: list[str]) -> str:
	stacks = move_crates_part2(data)
	return [crate.pop(0) for crate in stacks]

if __name__ == '__main__':
#	filename = 'test_data.txt'
	filename = 'puzzleinput.txt'
	data = read_file(filename)
	top_crates = show_top_crates(data)
	print(*top_crates, sep='')
	top_crates_part2 = show_top_crates_part2(data)
	print(*top_crates_part2, sep='')

